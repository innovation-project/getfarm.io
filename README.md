![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

Here's our repository for https://getfarm.io. This repository uses GitLab Pages and html5up to quickly and efficiently deploy our website website.

Note: The master branch of this repository goes straight to production. As such, it will be a protected branch.
